contextlinks.module
README.txt

The context links module allows you to easily create links to
context-related material using a simple markup syntax.  The links are
indicated by either linking the text itself or by inserting a specific
string or icon that links to the related information.

Site administrators can define different "classes" of context links.
Each class can have a specific icon or string that is used for all
instances of that class.  Additionally, administrators define a
default class which will be used if no class is specified.

The syntax to insert context links is as follows:

  [?<class>:<URL> <link text>]

or, to use the default class,

  [?:<URL> <link text>]


Notes:

  * The URL can be relative (some/link), linking to information on the
    same site, or fully-qualified (http://some.site/), linking to
    information on another site.

  * The URL can optionally be preceded with a ^ (caret) to force the
    link to open in a new window or a ` (backtick) to force the link
    to open in the current window (if the class defaults to opening in
    a new window).

  * To include a right bracket in the URL or text of a context link,
    it must be "escaped" by preceding it with a backslash (\]).
    Likewise, to use a backslash it must be escaped also (\\).


Files
  - contextlinks.module
      the actual module (PHP source code)

  - contextlinks.info
      the module information file used by Drupal

  - contextlinks.install
      installation/upgrade functions (PHP source code)

  - contextlinks.js
      JavaScript code used for opening links in a new window in an
      HTML4/XHTML-valid manner

  - contextlinks.png
      a screen shot of examples of the default context links classes

  - README.txt (this file)
      general module information

  - INSTALL.txt
      installation/configuration instructions

  - CREDITS.txt
      information on those responsible for this module

  - TODO.txt
      feature requests and modification suggestions

  - CHANGELOG.txt
      change/release history for this module

  - LICENSE.txt
      the license (GNU General Public License) covering the usage,
      modification, and distribution of this software and its
      accompanying files
