<?php

/**
 * The context links module provides a link -- internal or external --
 * to more information on a given word, phrase, or acronym.  Different
 * classes of these links can be created, each being identified by a
 * different icon, string, or both.  Examples of classes are help,
 * info, warning, etc.
 *
 * Links are inserted into content using markup that looks like this:
 * "[?:some/link The Link Text]" or
 * "[?info:http://some.external.site/link Other Link Text]".  The
 * former uses a relative link with the provided text and the default
 * class.  The latter uses an absolute link with the provided text,
 * using the "info" class.  The link can be created on the text itself
 * or by using an icon/image or string before or after the text.  The
 * text provided will be placed inside a span tag with a class of
 * "contextlinks contextlinks-<class>" so that text can be modified with style
 * sheets if desired.
 *
 * @copyright Copyright (c) 2004-2007 Jim Riggs.  All rights reserved.
 * @author Jim Riggs <drupal at jim and lissa dot com>
 */

/********************************************************************
 * Drupal Hooks
 ********************************************************************/

/**
 * Implementation of hook_perm().
 */
function contextlinks_perm() {
  return array('administer contextlinks');
}

/**
 * Implementation of hook_menu().
 */
function contextlinks_menu($may_cache) {
  $items = array();

  if (!$may_cache) {
    $path = drupal_get_path('module', 'contextlinks');

    drupal_add_css("$path/contextlinks.css");
    drupal_add_js("$path/contextlinks.js");
  }

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/contextlinks',
      'title' => t('Context links'),
      'description' => t('Create and configure context links classes.'),
      'access' => user_access('administer contextlinks'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('_contextlinks_admin_settings')
    );
    $items[] = array(
      'path' => 'admin/settings/contextlinks/overview',
      'title' => t('Overview'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10
    );
    $items[] = array(
      'path' => 'admin/settings/contextlinks/add',
      'title' => t('Add'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('_contextlinks_admin_form'),
      'type' => MENU_LOCAL_TASK
    );
    $items[] = array(
      'path' => 'admin/settings/contextlinks/edit',
      'title' => t('Edit'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('_contextlinks_admin_form'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'admin/settings/contextlinks/delete',
      'title' => t('Delete'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('_contextlinks_admin_delete'),
      'type' => MENU_CALLBACK
    );
  }

  return $items;
}

/**
 * Implementation of hook_filter().
 */
function contextlinks_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(t('Context links'));

    case 'description':
      return t('Allows filtered input of various types of informational, context-related links.');

    case 'process':
      return _contextlinks_process($text);

    default:
      return $text;
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function contextlinks_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    $output = t('
<h1 id="contextlinks">Context Links Help</h1>

<p>
  The context links module allows you to easily create links to
  context-related material using a simple markup syntax.  The links
  are indicated by either linking the text itself or by inserting a
  specific string or icon that links to the related information.
</p>

<p>
  Site administrators can define different "classes" of context
  links.  Each class can have a specific icon or string that is used
  for all instances of that class.  Additionally, administrators
  define a default class which will be used if no class is specified.
</p>

<p>
  The syntax to insert context links is as follows:
</p>

<p>
  <code>[?<i><b>class</b></i>:<i><b>URL</b></i> <i><b>link text</b></i>]</code><br />
  or, to use the default class,<br />
  <code>[?:<i><b>URL</b></i> <i><b>link text</b></i>]</code>
</p>

<p>
  Notes:
</p>

<ul>
  <li>The URL can be relative (some/link), linking to information on
  the same site, or fully-qualified (http://some.site/), linking to
  information on another site.</li>
  <li>The URL can optionally be preceded with a ^ (caret) to force the
  link to open in a new window or a ` (backtick) to force the link to
  open in the current window (if the class defaults to opening in a
  new window).</li>
  <li>To include a right bracket in the URL or text of a context
  link, it must be "escaped" by preceding it with a backslash (\\]).
  Likewise, to use a backslash it must be escaped also (\\\\).</li>
</ul>

<p>
  The table below shows examples of all of the classes currently
  defined on this site.
</p>
');

    $default = variable_get('contextlinks_default', NULL);
    $header = array(t('class'), t('new<br />window'), t('input'), t('output'));
    $rows = array();
    $result = db_query("SELECT cc.* FROM {contextlinks_class} cc ORDER BY cc.name");
    $example_text = t('example');

    while ($class = db_fetch_object($result)) {
      $text = t('An %example link.', array('%example' => "[?$class->name:filter/tips $example_text $class->name]"));

      if ($class->cid == $default) {
        $class->name .= '<br /><i><b>*' . t('default') . '*</b></i>';
        $text .= '<br />' . t('A %example using the default class.', array('%example' => '[?:filter/tips ' . t('link') . ']'));
      }

      $rows[] = array($class->name, ($class->new_window ? t('Yes') : t('No')), $text, _contextlinks_process($text));
    }

    return $output . theme('table', $header, $rows);
  }
  else {
    return t('You can use !context links in the text to create context-related links to pages or sites that provide additional information about a word or phrase.', array('!context links' => l(t('context links'), 'filter/tips', NULL, NULL, 'contextlinks')));
  }
}

/**
 * Implementation of hook_help().
 */
function contextlinks_help($section = 'admin/help#contextlinks') {
  switch ($section) {
    case 'admin/help#contextlinks':
      return t('
<p>
  The context links module allows users to create
  contextual/informational links within posts. The links may be
  created on the input text itself, or an icon, a string, or both may
  be insterted before or after the text and linked to the provided
  URL.
</p>

<p>
  Management of context links classes is handled using the
  admin/settings/contextlinks page. The "overview" provides a list
  of all context links classes and allows for editing and deleting
  existing classes.
</p>
');

    case 'admin/settings/contextlinks':
      return t('
<p>
  The table below the status table lists all context links classes on
  this site. Context links classes may be edited or deleted using the
  links provided.
</p>
');

    case 'admin/settings/contextlinks/add':
      return t('
<p>
  Use the form below to add a context links class to this site.
</p>
');
  }
}

/********************************************************************
 * Module Functions
 ********************************************************************/

/**
 * Returns the filtered version of the provided string, replacing all
 * context links markup with the appropriate linked images/text.
 *
 * @param $text
 *   The string specifying the text to be processed.
 *
 * @return
 *   A string containing the filtered version of the
 *   provided text.
 */
function _contextlinks_process($text) {
  return preg_replace_callback('/
                                 \[\?                   # opening bracket
                                 ([a-zA-Z0-9_-]*)       # $1 -- class name
                                 :                      # class, url delimiter
                                 (\^|`)?                # $2 -- optional bypass of link target
                                 ([^\s\]]+)             # $3 -- url
                                 \s+                    # url, text delimiter
                                 ((?:\\\\.|[^\]\\\\])+) # $4 -- text: escaped chars, not right bracket or backslash
                                 \]                     # closing bracket
                                /x', '_contextlinks_callback', $text);
}

/**
 * Returns the replacement text for the matching context links RE
 * provided.  Called as a callback from _contextlinks_process().
 *
 * @param $matches
 *   The array containing the matching subexpressions
 *   from the context links RE in _contextlinks_process().
 *
 * @return
 *   A string containing the replacement text appropriate
 *   for the provided matching subexpressions.
 */
function _contextlinks_callback($matches) {
  static $classes = FALSE;

  if ($classes === FALSE) {
    $classes = array();
  }

  $class = strtolower($matches[1]);

  switch ($matches[2]) {
    case '^':
      $rel = 'CONTEXTLINKS_NEW_WINDOW';
      break;

    case '`':
      $rel = '';
      break;
  }

  if (!$class) {
    $class = db_result(db_query("SELECT cc.name FROM {contextlinks_class} cc WHERE cc.cid = %d", variable_get('contextlinks_default', NULL)));
  }

  if ($classes[$class]) {
    $url = url(str_replace('{}', $matches[3], $classes[$class]['url_template']));
    $text = preg_replace('/\\\\(.)/', '$1', $matches[4]);

    $rel = (isset($rel) ? $rel : $classes[$class]['rel']);
    $rel = ($rel ? " rel=\"$rel\"" : '');

    return str_replace(array('{URL}', '{TARGET}', '{TEXT}'), array($url, $rel, $text), $classes[$class]['markup']);
  }

  $class = db_fetch_object(db_query("SELECT cc.* FROM {contextlinks_class} cc WHERE cc.name = '%s'", $class));

  if (!$class) {
    return $matches[0];
  }

  if ($class->image) {
    $class->image = url($class->image);
    $markup = "<img src=\"$class->image\" alt=\"$class->name\" />";
  }

  $matches2 = array();

  if ($class->string) {
    preg_match('/^([\(\[\{])?(.+?)([\)\]\}])?$/', $class->string, $matches2);

    $markup .= ($markup ? ' ' : '') . $matches2[2];
  }

  if (!$class->url_template || (strpos($class->url_template, '{}') === FALSE)) {
    $class->url_template .= '{}';
  }

  if (!$markup) {
    $markup = '<span class="contextlinks-text"><a href="{URL}"{TARGET}>{TEXT}</a></span>';
  }
  else {
    $text_mark = '<span class="contextlinks-text">' . ($class->link_text ? '<a href="{URL}"{TARGET}>{TEXT}</a>' : '{TEXT}') . '</span>';
    $markup = $matches2[1] . '<a href="{URL}"{TARGET}>' . $markup . '</a>' . $matches2[3];
    $markup = ($class->position ? "$text_mark $markup" : "$markup $text_mark");
  }

  $markup = "<span class=\"contextlinks contextlinks-$class->name\">$markup</span>";

  $classes[$class->name]['rel'] = ($class->new_window ? 'CONTEXTLINKS_NEW_WINDOW' : '');
  $classes[$class->name]['url_template'] = $class->url_template;
  $classes[$class->name]['markup'] = $markup;

  return _contextlinks_callback($matches);
}

/**
 * Displays the context links class table.
 *
 * @return
 *   An array containing the form elements to be displayed.
 */
function _contextlinks_admin_settings() {
  $result = db_query("SELECT cc.cid, cc.name FROM {contextlinks_class} cc ORDER BY cc.name");
  $names = array();

  while ($class = db_fetch_object($result)) {
    $names[$class->cid] = '';
  }

  $form = array();
  $form['contextlinks_default'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('contextlinks_default', NULL),
    '#options' => $names,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set default')
  );

  return $form;
}

/**
 * Sets the default class.
 *
 * @param $form_id
 *   The string specifying the form ID of the form that was submitted.
 * @param $form_values
 *   The array specifying the form values.
 */
function _contextlinks_admin_settings_validate($form_id, $form_values) {
  variable_set('contextlinks_default', $form_values['contextlinks_default']);
  cache_clear_all('*', 'cache_filter', TRUE);
  cache_clear_all('*', 'cache_page', TRUE);
}

/**
 * Renders the class list, including the default column.
 *
 * @param $form
 *   The array specifying the form to be rendered.
 *
 * @result
 *   The formatted HTML table of classes.
 */
function theme__contextlinks_admin_settings($form) {
  $positions = array(t('before'), t('after'));
  $header = array(t('Default'), array('data' => t('Class'), 'field' => 'name', 'sort' => 'asc'), t('URL template/image/text'), array('data' => t('Position'), 'field' => 'position'), array('data' => t('New Window'), 'field' => 'new_window'), array('data' => t('Link text'), 'field' => 'link_text'), array('data' => t('Operations')));
  $rows = array();
  $result = db_query("SELECT cc.* FROM {contextlinks_class} cc" . tablesort_sql($header));

  while ($class = db_fetch_object($result)) {
    $image = ($class->image ? url($class->image) : '');
    $rows[] = array(
      array('data' => drupal_render($form['contextlinks_default'][$class->cid]), 'align' => 'center'),
      $class->name,
      (($class->url_template ? $class->url_template : t('&lt;no template&gt;')) . '<br />' . ($class->image ? "$class->image <img src=\"$image\" alt=\"?\" style=\"width: 1.2em; height: 1.2em;\" />" : t('&lt;no image&gt;')) . '<br />' . ($class->string ? $class->string : t('&lt;no text&gt;'))), $positions[$class->position], ($class->new_window ? t('Yes') : t('No')), ($class->link_text ? t('Yes') : t('No')), l(t('edit'), "admin/settings/contextlinks/edit/$class->cid") . ' ' . l(t('delete'), "admin/settings/contextlinks/delete/$class->cid"));
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

/**
 * Creates a form for adding or editing a link class.
 *
 * @return
 *   A string containing the HTML-formatted form.
 */
function _contextlinks_admin_form($cid = 0) {
  if ($cid) {
    $class = db_fetch_object(db_query("SELECT cc.* FROM {contextlinks_class} cc WHERE cc.cid = %d", $cid));
  }

  $form = array();
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Class name'),
    '#default_value' => $class->name,
    '#size' => 16,
    '#maxlength' => 16,
    '#description' => t('The name of this class.  The name must be unique and can only contain letters, numbers, dash, and underscore.'),
    '#required' => TRUE
  );
  $form['url_template'] = array(
    '#type' => 'textfield',
    '#title' => t('URL template'),
    '#default_value' => $class->url_template,
    '#description' => t('Optional. The link template to be used for this class. For example, a template of %http://www.google.com/search?q= with a context link of [?google:foo search for foo] will result in a link of %http://www.google.com/search?q=foo by appending "foo" to the template. Alternatively, the placeholder %{} can be used to specify the location of the context link portion of the URL within the template (i.e. %http://www.google.com/search?q={}&hl=en&lr=lang_sr, %http://my.site/node/{}/edit).', array('%http://www.google.com/search?q=' => 'http://www.google.com/search?q=', '%http://www.google.com/search?q=foo' => 'http://www.google.com/search?q=foo', '%{}' => '{}', '%http://www.google.com/search?q={}&hl=en&lr=lang_sr' => 'http://www.google.com/search?q={}&hl=en&lr=lang_sr', '%http://my.site/node/{}/edit' => 'http://my.site/node/{}/edit'))
  );
  $form['link_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link text'),
    '#default_value' => $class->link_text,
    '#description' => t('Whether on not this class should make the text of the context link a link also. If neither an image nor a string is specified, this option will be implied.')
  );
  $form['new_window'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open new window'),
    '#default_value' => $class->new_window,
    '#description' => t('Whether on not this class should open links in a new window by default.  This setting can be overridden in any given link by preceding the URL with a ^ (new window) or ` (current window).')
  );
  $form['image_text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image and text'),
    '#description' => '<h3>' . t('Note') . '</h3>' . t('An image URL, text string, or both may be entered below. If present, these items will become the link. If neither is specified or if the "Link text" option is selected, the text of the context link itself will become the link.')
  );
  $form['image_text']['image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => $class->image,
    '#description' => t('The URL of the image to be displayed before or after this link class.')
  );
  $form['image_text']['string'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => $class->string,
    '#description' => t('A string to be displayed before or after this link class.  If desired, the string can be enclosed in brackets, braces, or parentheses.  Only the text inside these pairs will be linked.')
  );
  $form['image_text']['position'] = array(
    '#type' => 'radios',
    '#title' => t('Position'),
    '#default_value' => (isset($class->position) ? $class->position : 1),
    '#options' => array(t('Before'), t('After')),
    '#description' => t('Whether the link image or text string appears before or after the text.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $cid);

  return $form;
}

/**
 * Validates a new or modified link class.
 *
 * @param $form_id
 *   The form ID of the form data.
 * @param $form_values
 *   The form values passed from the form.
 */
function _contextlinks_admin_form_validate($form_id, $form_values) {
  if (!preg_match('/^[a-z0-9_-]+$/', trim($form_values['name']))) {
    form_set_error('name', t('Please enter a class name consisting of only a-z, 0-9, _, and -.'));
  }

  $template = str_replace('{}', 'foo', $form_values['url_template']);

  if (!valid_url($template, TRUE) && !valid_url($template, FALSE)) {
    form_set_error('url_template', t('Please enter a valid template URL.'));
  }

  if ($form_values['image'] && !valid_url($form_values['image'], TRUE) && !valid_url($form_values['image'], FALSE)) {
    form_set_error('image', t('Please enter a valid image URL.'));
  }
}

/**
 * Saves a new or modified link class into the database.
 *
 * @param $form_id
 *   The form ID of the form data.
 * @param $form_values
 *   The form values passed from the form.
 */
function _contextlinks_admin_form_submit($form_id, $form_values) {
  $position = ($form_values['position'] ? 1 : 0);
  $target = ($form_values['new_window'] ? 1 : 0);
  $link_text = (($form_values['link_text'] || (!$image && !$string)) ? 1 : 0);

  if ($form_values['cid']) {
    db_query("UPDATE {contextlinks_class} SET name = '%s', position = %d, new_window = %d, image = '%s', string = '%s', link_text = %d, url_template = '%s' WHERE cid = %d", $form_values['name'], $position, $target, $form_values['image'], $form_values['string'], $link_text, $form_values['url_template'], $form_values['cid']);
  }
  else {
    db_query("INSERT INTO {contextlinks_class} (cid, name, position, new_window, image, string, link_text, url_template) VALUES (%d, '%s', %d, %d, '%s', '%s', %d, '%s')", db_next_id('{contextlinks}_cid'), $form_values['name'], $position, $target, $form_values['image'], $form_values['string'], $link_text, $form_values['url_template']);
  }

  cache_clear_all('*', 'cache_filter', TRUE);
  cache_clear_all('*', 'cache_page', TRUE);
  drupal_set_message(t('Class %name successfully saved.', array('%name' => $name)));

  return 'admin/settings/contextlinks';
}

/**
 * Deletes a context links class.
 *
 * @param $cid
 *   The class ID of the class to delete.
 */
function _contextlinks_admin_delete($cid) {
  $class = db_result(db_query('SELECT cc.name FROM {contextlinks_class} cc WHERE cc.cid = %d', $cid));

  $form = array();
  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $cid
  );
  $form['class_name'] = array(
    '#type' => 'value',
    '#value' => $class
  );

  return confirm_form($form, t('Are you sure you want to permanently delete the context links class %name?', array('%name' => $class)), 'admin/settings/contextlinks', t('Any existing content that uses this class will not be formatted. This cannot be undone.'), t('Delete'));
}

/**
 * Deletes the specified class.
 *
 * @param $form_id
 *   The string specifying the form ID of the form that was submitted.
 * @param $form_values
 *   The array specifying the form values.
 */
function _contextlinks_admin_delete_submit($form_id, $form_values) {
  db_query("DELETE FROM {contextlinks_class} WHERE cid = %d", $form_values['cid']);
  cache_clear_all('*', 'cache_filter', TRUE);
  cache_clear_all('*', 'cache_page', TRUE);
  drupal_set_message(t('Class %name successfully deleted.', array('%name' => $form_values['class_name'])));

  return 'admin/settings/contextlinks';
}

?>