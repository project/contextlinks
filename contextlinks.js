
/**
 * The contextlinks.js file provides a JavaScript function for opening
 * links in a new browser window.  Links with the rel attribute set to
 * "CONTEXTLINKS_NEW_WINDOW" will be opened in a new browser.  This is
 * used instead of the target attribute to allow strict HTML4 and
 * XHTML pages to validate.
 *
 * @copyright Copyright (c) 2004-2007 Jim Riggs.  All rights reserved.
 * @author Jim Riggs <drupal at jim and lissa dot com>
 */

if (Drupal.jsEnabled) {
  $(document).ready(function() { $('a[@rel=CONTEXTLINKS_NEW_WINDOW]').attr('target', '_blank'); });
}
